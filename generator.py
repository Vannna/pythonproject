import random
import datetime
import calendar
from openpyxl import Workbook

womanNames = ['Анна', 'Анастасия', 'Елена', 'Марина', 'Дарья']
manNames = ['Алексей', 'Олег', 'Николай', 'Иннокентий', 'Кузьма']
lastNames = ['Приходько', 'Махно', 'Степаненко', 'Клименко', 'Романенко']
users = []
userCount = 10000
minIncome = 18000
maxIncome = 80000
minYear = 1960
maxYear = 2000
maxMonth = 12
maxDay = 31


class User:
    key = 0
    # pair of first name and last name
    name = ''
    #0 - woman, 1 - men
    sex = 0
    income = 0
    birthDate = datetime.date.today()

def printUser(user):
    print(str(user.key) + ' ' + str(user.birthDate) + user.name + ' ' + str(user.income) + ', Sex : ' + str(user.sex))

def fillInUsers(count):
    for index in range(1, count):
        tempUser = User()
        tempUser.key = index
        tempUser.sex = random.randint(0, 1)
        tempUser.income = random.randint(minIncome, maxIncome)
        
        if tempUser.sex == 0:
            tempUser.name = womanNames[random.randint(1, len(womanNames) - 1)]
        else:
            tempUser.name = manNames[random.randint(1, len(manNames) - 1)]
        
        tempUser.name += ' ' + lastNames[random.randint(1, len(lastNames)-1)]

        tempYear = random.randint(minYear, maxYear)
        tempMonth = random.randint(1, maxMonth)
        tempDay = calendar.monthrange(tempYear, tempMonth)[1]

        tempUser.birthDate = datetime.date(tempYear, tempMonth, tempDay)

        users.append(tempUser)

def printResult(option):
    # 1. max income
    if(option == 1):
        print(max(user.income for user in users))

    # 2. max age
    if(option == 2):
        print(max(user.birthDate for user in users))

    # 3. min age
    if(option == 3):
        print(min(user.income for user in users))

    # 4. count of womans
    if(option == 4):
        print(sum(1 for user in users if user.sex == 0))

    # 5. count of mens
    if(option == 5):
        print(sum(1 for user in users if user.sex == 1))

    # 6. employees where age > 45
    if(option == 6):
        for user in users:
            if datetime.date.today().year - user.birthDate.year >= 45:
                printUser(user)

    # 7. employees where age < 25
    if(option == 7):
        for user in users:
            if datetime.date.today().year - user.birthDate.year <= 25:
                printUser(user)

    # 8. mens where age > 40
    if(option == 8):
        for user in users:
            if user.sex == 1 and datetime.date.today().year - user.birthDate.year >= 45:
                printUser(user)

    # 9. mens where age < 30
    if(option == 9):
        for user in users:
            if user.sex == 1 and datetime.date.today().year - user.birthDate.year <= 30:
                printUser(user)

    # 10. womans where age > 40
    if(option == 10):
        for user in users:
            if user.sex == 0 and datetime.date.today().year - user.birthDate.year >= 40:
                printUser(user)

    # 11. womans where age < 25
    if(option == 11):
        for user in users:
           if user.sex == 0 and datetime.date.today().year - user.birthDate.year <= 25:
              printUser(user)

    # 12. average age of low and high age
    if(option == 12):
        print(min(user.birthDate for user in users))
        print(max(user.birthDate for user in users))

    # 13. average income of all, low and high income
    if(option == 13):
        print(max(user.income for user in users))
        print(min(user.income for user in users))

wb = Workbook()
ws = wb.active

fillInUsers(userCount)

index = 0
for user in users:
    ws.append([user.key, user.name, user.sex, user.income, user.birthDate])
    index+=1

printResult(int(input('Choose task\n')))

wb.save("database.xls")